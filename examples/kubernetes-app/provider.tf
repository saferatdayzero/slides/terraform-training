# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "minikube"
}
