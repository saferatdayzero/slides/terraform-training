# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

MARP_SLIDES = $(shell find -name slides.md)
MARP_HTML_SLIDES = $(patsubst %.md,%.html,$(MARP_SLIDES))
MARP_PDF_SLIDES = $(patsubst %.md,%.pdf,$(MARP_SLIDES))

DRAWIO_FILES = $(shell find -name \*.drawio)
DRAWIO_PNG_FILES = $(patsubst %.drawio,%.png,$(DRAWIO_FILES))

.PHONY: all
all: html pdf

.PHONY: drawio
drawio: $(DRAWIO_PNG_FILES)

.PHONY: html
html: $(MARP_SLIDES)
	marp --html $<

.PHONY: pdf
pdf: $(MARP_SLIDES)
	marp --html --allow-local-files --pdf $<

.PHONY: clean
clean:
	rm -f $(DRAWIO_PNG_FILES) $(MARP_HTML_SLIDES) $(MARP_PDF_SLIDES)

%.png: %.drawio
	drawio -x -f png -s 2 "$<" -o "$@"

$(MARP_SLIDES): $(DRAWIO_PNG_FILES)
